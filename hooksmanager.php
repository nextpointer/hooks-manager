<?php
/**
* 2018 Nexpointer.gr
*
* NOTICE OF LICENSE
*
* This source file is subject to the General Public License (GPL 2.0)
* that is bundled with this package in the file LICENSE.txt.
* It is also available through the world-wide-web at this URL:
* http://opensource.org/licenses/GPL-2.0
*
* DISCLAIMER
*
* Do not edit or add to this file if you wish to upgrade the module to newer
* versions in the future.
*
*  @author    nextpointer.gr <support@nextpointer.gr>
*  @copyright 2018 nextpointer.gr
*  @license   http://opensource.org/licenses/GPL-2.0 General Public License (GPL 2.0)
*  @version   1.0.0
*  @created   02 September 2018
*  @last updated 02 September 2018
*/

if (!defined('_PS_VERSION_')) {
    exit;
}

class Hooksmanager extends Module
{
    public function __construct()
    {
        $this->name = 'hooksmanager';
        $this->tab = 'administration';
        $this->version = '1.0.0';
        $this->author = 'Evanggelos L. Goritsas';
        $this->need_instance = 0;

        $this->bootstrap = true;
        parent::__construct();

        $this->displayName = $this->l('Hooks manager');
        $this->description = $this->l('Manage hooks. Add and Remove any hook you want. ');
        $this->ps_versions_compliancy = array('min' => '1.7', 'max' => _PS_VERSION_);
    }

    public function install()
    {
        $this->adminInstall();

        return parent::install();
    }

    public function uninstall()
    {
        $this->adminUnistall();

        return parent::uninstall();
    }


    private function adminInstall()
    {
        $id_tab = Tab::getIdFromClassName('AdminParentThemes');

        if ($id_tab) {
            $tab = new Tab();
            $tab->class_name = 'AdminHooksManager';
            $tab->module = $this->name;
            $tab->id_parent = $id_tab;
            $languages = Language::getLanguages(false);

            foreach ($languages as $language) {
                $tab->name[$language['id_lang']] = 'Hooks Manager';
            }

            if (!$tab->save()) {
                return false;
            }
        }
        return true;
    }

    private function adminUnistall()
    {
        $idFromClassName = Tab::getIdFromClassName('AdminHooksManager');
        $tab = new Tab($idFromClassName);
        $tab->delete();
    }
}
